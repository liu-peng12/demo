package com.lp.demo.mapper;

import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.BookPublish;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookPublishMapper {
    List<BookPublish> queryBookPublish();
    boolean addBookPublish(BookPublish bookPublish);
    boolean updBookPublish(BookPublish bookpublish);
    boolean delBookPublish(int publishid);
}
