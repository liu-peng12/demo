package com.lp.demo.mapper;

import com.lp.demo.pojo.Admins;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface RegisterMapper {
    public boolean Register(int adminname,int adminpassword);
    public Admins queryById(int adminname, int adminpassword);
}
