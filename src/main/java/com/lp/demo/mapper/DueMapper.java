package com.lp.demo.mapper;

import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Due;
import org.apache.ibatis.annotations.Mapper;

import java.util.Date;
import java.util.List;

@Mapper
public interface DueMapper {
//    查询所有还书记录
    List<Due> queryDue();
//    还书操作，首先查询借书表是否存在，如果存在删除该借书表，图书+1，还书表+一条数据
    int returnDue(Integer bookid, Integer studentid, Date duedate);

//    查询借书表是否存在数据
    List<Borrow> queryBorrows(int bookid, int studentid);
//    删除借书表数据
    int delBorrow(Integer bookid,Integer studentid);
}
