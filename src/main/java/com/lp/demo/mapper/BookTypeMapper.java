package com.lp.demo.mapper;

import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.BookType;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookTypeMapper {
    List<BookType> queryBookTypes();
    boolean addBookType(BookType bookType);
    boolean updBookType(BookType bookType);
    boolean delBookType(int tid);
}
