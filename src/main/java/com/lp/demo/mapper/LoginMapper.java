package com.lp.demo.mapper;

import com.lp.demo.pojo.Admins;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface LoginMapper {
    Admins Login(int adminname,int adminpassword);
}
