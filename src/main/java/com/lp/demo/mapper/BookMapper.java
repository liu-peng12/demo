package com.lp.demo.mapper;

import com.lp.demo.pojo.Book;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface BookMapper {
//    增删改查
    public List<Book> queryBook();
    public boolean addBook(Book b);
    public boolean updBook(Book b);
    public boolean delBook(Integer bookid);

//    按照图书分类查询
    public List<Book> queryBookType(Integer booktype);
//    按照图书名查询
    public List<Book> queryBookName(String title);
//    根据图书id查询
    public Book queryBookId(int bookid);
}
