package com.lp.demo.mapper;

import com.lp.demo.controller.StudentController;
import com.lp.demo.pojo.Admins;
import com.lp.demo.pojo.Students;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface StudentMapper {
    public List<Students> querystu();
    public boolean addstu(Students stu);
    public boolean updstu(Students i);
    public boolean delstu(int studentid);
    public List<Students> querystuBySno(int sno);
    public List<Students> querystuByName(String name);
//    批量删除
    public boolean delManyStu(int sno);
//    批量添加
    public boolean addManyStu(int studentid,int sno,int password);
//    批量添加2.0 （List版）
    public boolean addManyStus(List<Students> students);
}