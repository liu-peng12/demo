package com.lp.demo.mapper;

import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Students;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

@Mapper
public interface BorrowMapper {
//    查询当前图书是否存在以及可借阅数量是否>0
    Book queryBorrowBook(int bookid);
//    借阅图书库中-1
    int updBorrowBook(int bookid,int quantity);
//    添加借阅图书相关信息
    int addBorrowBook(int bookid, int studentid, @DateTimeFormat(pattern = "yyyy-MM-dd") Date borrowdate, @DateTimeFormat(pattern = "yyyy-MM-dd")Date returndate);
//    查询所有借阅信息
    List<Borrow> queryBorrow();
//    根据学生学号查询借阅图书记录（首先根据学号调用）
    Students queryBorrowSno(int sno);
//    根据学生id查询
    List<Borrow> queryBorrowStuId(int studentid);

//    根据学生学号查询学生id
    int querySno(int sno);

}
