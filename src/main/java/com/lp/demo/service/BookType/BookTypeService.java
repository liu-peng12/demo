package com.lp.demo.service.BookType;

import com.lp.demo.pojo.BookType;

import java.util.List;

public interface BookTypeService {
    List<BookType> queryBookTypes();
    boolean addBookType(BookType bookType);
    boolean updBookType(BookType bookType);
    boolean delBookType(int tid);
}
