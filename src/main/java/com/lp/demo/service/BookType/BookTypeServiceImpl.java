package com.lp.demo.service.BookType;

import com.lp.demo.mapper.BookTypeMapper;
import com.lp.demo.pojo.BookType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.websocket.server.ServerEndpoint;
import java.util.List;

@SuppressWarnings("all")
@Service
public class BookTypeServiceImpl implements BookTypeService {
    @Autowired
    BookTypeMapper btm;
    @Override
    public List<BookType> queryBookTypes()
    {
        return btm.queryBookTypes();
    }
    @Override
    public boolean addBookType(BookType bookType){return btm.addBookType(bookType);
    }
    @Override
    public boolean updBookType(BookType bookType){return btm.updBookType((bookType));}
    @Override
    public boolean delBookType(int tid){return btm.delBookType(tid);}
}
