package com.lp.demo.service.Student;

import com.lp.demo.mapper.StudentMapper;
import com.lp.demo.pojo.Students;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings("all")
@Service
public class StudentServiceImpl implements StudentService{
    @Autowired
    StudentMapper sm;
    @Override
    public List<Students> querystu(){return sm.querystu();}
    @Override
    public boolean addstu(Students stu){return sm.addstu(stu);}
    @Override
    public boolean updstu(Students stu){return sm.updstu(stu);}
    @Override
    public boolean delstu(int studentid){return sm.delstu(studentid);}
    @Override
    public List<Students> querystuBySno(int sno){return sm.querystuBySno(sno);}
    @Override
    public List<Students> querystuByName(String name){return sm.querystuByName(name);}

    @Override
    public boolean delManyStu(int sno) {
       return sm.delManyStu(sno);
    }
    @Override
    public boolean addManyStu(int studentid,int sno,int password)
    {
        return sm.addManyStu(studentid,sno,password);
    }
    @Override
    public boolean addManyStus(List<Students> students)
    {
        return sm.addManyStus(students);
    }
}
