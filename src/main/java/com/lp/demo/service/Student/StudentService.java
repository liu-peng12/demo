package com.lp.demo.service.Student;

import com.lp.demo.pojo.Students;

import java.util.List;

public interface StudentService {
    public List<Students> querystu();
    public boolean addstu(Students stu);
    public boolean updstu(Students stu);
    public boolean delstu(int studentid);
    public List<Students> querystuBySno(int sno);
    public List<Students> querystuByName(String name);
    public boolean delManyStu(int sno);
    public boolean addManyStu(int studentid,int sno,int password);
    public boolean addManyStus(List<Students> students);
}
