package com.lp.demo.service;

import com.lp.demo.mapper.LoginMapper;
import com.lp.demo.pojo.Admins;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@SuppressWarnings("all")
@Service
public class LoginServiceImpl implements LoginService {
    @Autowired
    LoginMapper lm;
    @Override
    public String Login(int adminname, int adminpassword)
    {
        Admins admin=lm.Login(adminname,adminpassword);
        if(admin==null)
        {
            return "用户名或密码不对，请重新输入";
        }
        else{
            return "登录成功";
        }
    }
}
