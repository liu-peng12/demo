package com.lp.demo.service.Borrow;

import com.lp.demo.mapper.BorrowMapper;
import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Students;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
@SuppressWarnings("all")
public class BorrowServiceImpl implements BorrowService{
    @Autowired
    BorrowMapper bm;
    @Override
    public Book queryBorrowBook(int bookid){return bm.queryBorrowBook(bookid);}
    @Override
    public int updBorrowBook(int bookid,int quantity){return bm.updBorrowBook(bookid,quantity);}
    @Override
    public int addBorrowBook(int bookid, int studentid, Date borrowdate, Date returndate)
    {return bm.addBorrowBook(bookid,studentid,borrowdate,returndate);}
    @Override
    public List<Borrow> queryBorrow()
    {return bm.queryBorrow();}
    @Override
    public Students queryBorrowSno(int sno)
    {return bm.queryBorrowSno(sno);}
    @Override
    public List<Borrow> queryBorrowStuId(int studentid)
    {return bm.queryBorrowStuId(studentid);}
    @Override
    public int querySno(int sno){return bm.querySno(sno);}
}
