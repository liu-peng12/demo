package com.lp.demo.service.Borrow;

import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Students;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

public interface BorrowService {
    Book queryBorrowBook(int bookid);
    int updBorrowBook(int bookid,int quantity);
    int addBorrowBook(int bookid, int studentid, @DateTimeFormat(pattern = "yyyy-MM-dd") Date borrowdate, @DateTimeFormat(pattern = "yyyy-MM-dd")Date returndate);
    List<Borrow> queryBorrow();
    Students queryBorrowSno(int sno);
    List<Borrow> queryBorrowStuId(int studentid);
    int querySno(int sno);
}
