package com.lp.demo.service.Book;

import com.lp.demo.mapper.BookMapper;
import com.lp.demo.pojo.Book;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings("all")
@Service
public class BookServiceImpl implements BookService {
    @Autowired
    BookMapper bm;
    @Override
    public List<Book> queryBook(){return bm.queryBook();}
    @Override
    public boolean addBook(Book book){return bm.addBook(book);}
    @Override
    public boolean updBook(Book book){return bm.updBook(book);}
    @Override
    public boolean delBook(Integer bookid){return bm.delBook(bookid);}
    @Override
    public List<Book> queryBookType(Integer booktype){return bm.queryBookType(booktype);}
    @Override
    public List<Book> queryBookName(String title){return bm.queryBookName(title);}
    @Override
    public Book queryBookId(int bookid){return bm.queryBookId(bookid);}
}
