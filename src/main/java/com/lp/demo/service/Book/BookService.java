package com.lp.demo.service.Book;

import com.lp.demo.pojo.Book;

import java.util.List;

public interface BookService {
    public List<Book> queryBook();
    public boolean addBook(Book book);
    public boolean updBook(Book book);
    public boolean delBook(Integer bookid);

    public List<Book> queryBookType(Integer booktype);
    public List<Book> queryBookName(String title);

    public Book queryBookId(int bookid);
}
