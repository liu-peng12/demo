package com.lp.demo.service.Register;

import com.lp.demo.mapper.RegisterMapper;
import com.lp.demo.pojo.Admins;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@SuppressWarnings("all")
@Service
public class RegisterServiceImpl implements RegisterService {
    @Autowired
    RegisterMapper rm;
    @Override
    public boolean Register(int adminname,int adminpassword)
    {

        if(rm.queryById(adminname,adminpassword)==null)
        {
            rm.Register(adminname,adminpassword);
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public Admins queryById(int adminname,int adminpassword)
    {
        return rm.queryById(adminname,adminpassword);
    }
}
