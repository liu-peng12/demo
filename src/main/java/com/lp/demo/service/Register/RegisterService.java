package com.lp.demo.service.Register;

import com.lp.demo.pojo.Admins;

public interface RegisterService {
    boolean Register(int adminname,int adminpassword);
    Admins queryById(int adminname, int adminpassword);
}
