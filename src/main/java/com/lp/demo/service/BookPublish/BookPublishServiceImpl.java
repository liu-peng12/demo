package com.lp.demo.service.BookPublish;

import com.lp.demo.mapper.BookPublishMapper;
import com.lp.demo.pojo.BookPublish;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@SuppressWarnings("all")
@Service
public class BookPublishServiceImpl implements BookPublishService{
    @Autowired
    BookPublishMapper bpm;
    @Override
    public List<BookPublish> queryBookPublish()
    {
        return bpm.queryBookPublish();
    }
    @Override
    public boolean addBookPublish(BookPublish bookPublish)
    {
       return bpm.addBookPublish(bookPublish);
    }

    @Override
    public boolean updBookPublish(BookPublish bookPublish) {
        return bpm.updBookPublish(bookPublish);
    }

    @Override
    public boolean delBookPublish(int publishid) {
        return bpm.delBookPublish(publishid);
    }

}
