package com.lp.demo.service.BookPublish;

import com.lp.demo.pojo.BookPublish;

import java.util.List;

public interface BookPublishService {
    List<BookPublish> queryBookPublish();
    boolean addBookPublish(BookPublish bookPublish);
    boolean updBookPublish(BookPublish bookPublish);
    boolean delBookPublish(int publishid);
}
