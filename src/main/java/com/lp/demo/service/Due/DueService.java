package com.lp.demo.service.Due;

import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Due;

import java.util.Date;
import java.util.List;

public interface DueService {
    List<Due> queryDue();
    int returnDue(int bookid, int studentid, Date duedate);

    int delBorrow(int bookid,int studentid);
    List<Borrow> queryBorrows(int bookid,int studentid);
}
