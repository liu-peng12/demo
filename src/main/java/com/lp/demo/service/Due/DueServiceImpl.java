package com.lp.demo.service.Due;

import com.lp.demo.mapper.DueMapper;
import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Due;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@SuppressWarnings("all")
@Service
public class DueServiceImpl implements DueService{
    @Autowired
    DueMapper dm;
    @Override
    public List<Due> queryDue(){
        return dm.queryDue();
    }
    @Override
    public int returnDue(int bookid, int studentid, Date duedate)
    {return dm.returnDue(bookid,studentid,duedate);}
    @Override
    public int delBorrow(int bookid,int studentid)
    {
        return dm.delBorrow(bookid,studentid);
    }
    @Override
    public List<Borrow> queryBorrows(int bookid,int studentid)
    {
        return dm.queryBorrows(bookid,studentid);
    }
}
