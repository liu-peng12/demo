package com.lp.demo.pojo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Borrow {
    private Integer borrowid;
    private Integer bookid;
    private Integer studentid;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date borrowdata;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date dueDate;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date returndate;
    private Integer sno;
    private String name;
}
