package com.lp.demo.pojo;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

@Data
public class Due {
    private Integer did;
    private Integer studentid;
    private String bookid;
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date duedate;
    private String name;
    private String title;
}
