package com.lp.demo.pojo;

import lombok.Data;

@Data
public class BookPublish {
    private Integer publishid;
    private String publishtitle;
    private Integer phone;
    private String address;
}
