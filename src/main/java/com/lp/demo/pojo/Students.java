package com.lp.demo.pojo;

import lombok.Data;

@Data
public class Students {
    private int studentid;
    private int sno;
    private String name;
    private int password;
}
