package com.lp.demo.pojo;

import lombok.Data;

@Data
public class BookType {
    private int tid;
    private String tname;
}
