package com.lp.demo.pojo;

import lombok.Data;

@Data
public class Book {
    private Integer bookid;
    private String title;
    private String author;
    private Integer pubid;
    private Integer quantity;
    private Integer booktype;

    private String publishtitle;
    private String tname;
}
