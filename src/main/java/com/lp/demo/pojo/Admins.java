package com.lp.demo.pojo;

import lombok.Data;

@Data
public class Admins {
    private Integer id;
    private Integer adminname;
    private Integer adminpassword;
}
