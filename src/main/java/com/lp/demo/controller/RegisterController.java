package com.lp.demo.controller;

import com.lp.demo.pojo.Admins;
import com.lp.demo.service.Register.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RegisterController {
    @Autowired
    RegisterService rs;
    @RequestMapping("Register")
    public boolean Register(int adminname,int adminpassword)
    {
        return rs.Register(adminname,adminpassword);
    }
    @RequestMapping("queryById")
    public Admins queryById(int adminname, int adminpassword)
    {
        return rs.queryById(adminname,adminpassword);
    }
}
