package com.lp.demo.controller;

import com.lp.demo.mapper.DueMapper;
import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Due;
import com.lp.demo.service.Borrow.BorrowService;
import com.lp.demo.service.Due.DueService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;

@RestController
public class DueController {
    @Autowired
    DueService ds;
    @Autowired
    BorrowService bs;
    @RequestMapping("queryDue")
    public List<Due> queryDue()
    {return ds.queryDue();}
    @RequestMapping("returnDue")
    public int returnDue(int bookid, int studentid,@DateTimeFormat(pattern = "yyyy-MM-dd")@RequestParam("duedate") Date duedate)
    {
//        查询借书表
        if(ds.queryBorrows(bookid,studentid)!=null)
        {
//            删除借书表
            ds.delBorrow(bookid,studentid);
            Book b=bs.queryBorrowBook(bookid);
//            根据图书id来让数量在源基础+1
            bs.updBorrowBook(bookid,b.getQuantity()+1);
//            还书表+1条数据
            return ds.returnDue(bookid,studentid,duedate);
        }
        return 0;
    }
}
