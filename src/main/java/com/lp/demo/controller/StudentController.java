package com.lp.demo.controller;

import com.lp.demo.mapper.StudentMapper;
import com.lp.demo.pojo.Students;
import com.lp.demo.service.Student.StudentService;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class StudentController {
    @Autowired
    StudentService ss;
    @RequestMapping("querystu")
    public List<Students> querystu(){return ss.querystu();}
    @RequestMapping("addstu")
    public boolean addstu(Students stu){return ss.addstu(stu);}
    @RequestMapping("updstu")
    public boolean updstu(Students stu){return ss.updstu(stu);}
    @RequestMapping("delstu")
    public boolean delstu(int studentid){return ss.delstu(studentid);}
    @RequestMapping("querystuBySno")
    public List<Students> querystuBySno(int sno){return ss.querystuBySno(sno);}
    @RequestMapping("querystuByName")
    public List<Students> querystuByName(String name){return ss.querystuByName(name);}
    @RequestMapping("delManyStu")
    public boolean delBookStu(int sno){return ss.delManyStu(sno);}
    @RequestMapping("addManyStu")
    public boolean addManyStus(int baseData,int mins,int maxs){
        int sno;
        int password=888888;
        for (int i = mins; i <=maxs; i++) {
            sno = baseData * 100 + i;
            System.out.println(sno);
            ss.addManyStu(sno,sno, password);
        }
        return true;
    }
    @PostMapping("addmManyStus")
    public boolean addManyStus(List<Students> students)
    {
        return ss.addManyStus(students);
    }

}
