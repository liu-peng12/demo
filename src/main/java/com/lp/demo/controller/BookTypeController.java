package com.lp.demo.controller;

import com.lp.demo.pojo.BookType;
import com.lp.demo.service.BookType.BookTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookTypeController {
    @Autowired
    BookTypeService bs;
    @RequestMapping("/queryBookTypes")
    public List<BookType> queryBookTypes()
    {
        return bs.queryBookTypes();
    }
    @RequestMapping("/addBookType")
    public boolean addBookType(BookType bookType){return bs.addBookType(bookType);}
    @RequestMapping("/updBookType")
    public boolean updBookType(BookType bookType){return bs.updBookType(bookType);}
    @RequestMapping("/delBookType")
    public boolean delBookType(int tid){return bs.delBookType(tid);}
}
