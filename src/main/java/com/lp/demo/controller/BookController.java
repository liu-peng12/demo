package com.lp.demo.controller;

import com.lp.demo.pojo.Book;
import com.lp.demo.service.Book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookController {
    @Autowired
    BookService bs;
    @RequestMapping("queryBook")
    public List<Book> queryBook(){return bs.queryBook();}
    @RequestMapping("addBook")
    public boolean addBook(Book book){return bs.addBook(book);}
    @RequestMapping("updBook")
    public boolean updBook(Book book){return bs.updBook(book);}
    @RequestMapping("delBook")
    public boolean delBook(Integer bookid){return bs.delBook(bookid);}
    @RequestMapping("queryBookType")
    public List<Book> queryBooktype(int booktype){return bs.queryBookType(booktype);}
    @RequestMapping("queryBookName")
    public List<Book> queryBookName(String title){return bs.queryBookName(title);}
    @RequestMapping("queryBookId")
    public Book queryBookId(int bookid){return bs.queryBookId(bookid);}
}