package com.lp.demo.controller;

import com.lp.demo.pojo.BookPublish;
import com.lp.demo.service.BookPublish.BookPublishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BookPublishController {
    @Autowired
    BookPublishService bps;
    @RequestMapping("queryBookPublish")
    public List<BookPublish> queryBookPublish()
    {
        return bps.queryBookPublish();
    }
    @RequestMapping("addBookPublish")
    public boolean addBookPublish(BookPublish bookPublish)
    {
        return bps.addBookPublish(bookPublish);
    }
    @RequestMapping("updBookPublish")
    public boolean updBookPublish(BookPublish bookPublish){return bps.updBookPublish(bookPublish);}
    @RequestMapping("delBookPublish")
    public boolean delBookPublish(int publishid){return bps.delBookPublish(publishid);}
}
