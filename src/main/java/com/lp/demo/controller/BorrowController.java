package com.lp.demo.controller;

import com.lp.demo.pojo.Book;
import com.lp.demo.pojo.Borrow;
import com.lp.demo.pojo.Students;
import com.lp.demo.service.Borrow.BorrowService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
public class BorrowController {
    @Autowired
    BorrowService bs;
    @RequestMapping("queryBorrowBook")
    public Boolean queryBorrowBook(@RequestParam("bookid") int bookid, @RequestParam("studentid") int studentid, @RequestParam("borrowdate")@DateTimeFormat(pattern = "yyyy-MM-dd") Date borrowdate,@RequestParam("returndate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date returndate){
////        System.out.println(bs.queryBorrowBook(bookid));
        if(bs.queryBorrowBook(bookid)!=null)
        {
            Book b=bs.queryBorrowBook(bookid);
            int quantity=b.getQuantity()-1;
            bs.updBorrowBook(bookid,quantity);
            bs.addBorrowBook(bookid,studentid,borrowdate,returndate);
            return true;
        }
        return false;
    }
    @RequestMapping("queryBorrow")
    public List<Borrow> queryBorrow()
    {
        return bs.queryBorrow();
    }
    @RequestMapping("queryBorrowSno")
    public List<Borrow> queryBorrowSno(int sno)
    {
        if(bs.queryBorrowSno(sno)!=null)
        {
            Students stu=bs.queryBorrowSno(sno);
            return bs.queryBorrowStuId(stu.getStudentid());
        }
        return null;
    }
    @RequestMapping("querySno")
    public int querySno(int sno){
        return bs.querySno(sno);
    }
}