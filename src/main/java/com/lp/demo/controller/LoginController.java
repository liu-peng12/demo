package com.lp.demo.controller;

import com.lp.demo.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class LoginController {
    @Autowired
    LoginService ls;
    @RequestMapping("test")
    public String test()
    {
        return "start SpringBoot";
    }
    @RequestMapping("Login")
    public String Login(int adminname,int adminpassword)
    {
        return ls.Login(adminname,adminpassword);
    }
}
